package com.example.lab3;

import com.example.lab3.exceptions.BadRequestExcep;
import com.example.lab3.exceptions.NotFoundException;
import com.example.lab3.exceptions.UnauthorizedException;
import com.example.lab3.exceptions.UserAlreadyExists;
import com.example.lab3.model.Error;
import com.example.lab3.model.ResponseHeader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import java.time.LocalDateTime;

import java.util.UUID;

@ControllerAdvice
public class ExceptionMapper {

    @ExceptionHandler({UserAlreadyExists.class})
    public ResponseEntity<Error> handleBusinessException() {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(LocalDateTime.now()))
                .code("ALREADY_EXISTS")
                .message("User already exists"));
    }
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Error> handleNotFoundException(NotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(LocalDateTime.now()))
                .code("NOT_FOUND")
                .message("Resource doesn't exist"));
    }

    @ExceptionHandler({BadRequestExcep.class})
    public ResponseEntity<Error> handleBadRequest(BadRequestExcep ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(LocalDateTime.now()))
                .code("BAD_REQUEST")
                .message("400 Bad request"));
    }

    @ExceptionHandler({UnauthorizedException.class})
    public ResponseEntity<Error> handleUnauthorizedException(UnauthorizedException ex) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(LocalDateTime.now()))
                .code("UNAUTHORIZED_ACCESS")
                .message("Incorrect username or password."));
    }

    @ExceptionHandler({RuntimeException.class, Exception.class})
    public ResponseEntity<Error> handleException(RuntimeException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(LocalDateTime.now()))
                .code("BAD_REQUEST")
                .message("Bad request."));
    }


}

