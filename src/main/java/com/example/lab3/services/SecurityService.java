package com.example.lab3.services;

import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Service
public class SecurityService {

    public String[] extractAndDecodeBasicAuth(String authorizationHeader){

        if(authorizationHeader == null || !authorizationHeader.startsWith("Basic ")){
            return null;
        }

        byte[] decodedBytes = Base64.getDecoder().decode(authorizationHeader.split(" ")[1]);
        String decodedString = new String(decodedBytes, StandardCharsets.UTF_8);

        return decodedString.split(":", 2);
    }

}
